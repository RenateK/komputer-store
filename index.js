import { formatNumberToNOK } from "./utils.js"
import {
  computerInfoTemplate,
  computerOptionTemplate,
  computerSpecsTemplate,
} from "./views/computerTemplates.js"

// Bank elements
const bankBalanceElement = document.getElementById("bank-balance")
const outstandingLoanElement = document.getElementById("outstanding-loan")
const loanBtn = document.getElementById("loan-btn")
const repayBtn = document.getElementById("repay-btn")

// Work elements
const salaryElement = document.getElementById("salary")
const workBtn = document.getElementById("work-btn")
const transferBtn = document.getElementById("transfer-btn")

//Computer elements
const computerSelect = document.getElementById("computer-select")
const infoContainer = document.getElementById("info-content")
const specsList = document.getElementById("specs-list")
const buyBtn = document.getElementById("buy-btn")

// Variables
const baseURL = "https://noroff-komputer-store-api.herokuapp.com/"
let computers = []
let salary = 0
let bankBalance = 200
let hasOutstandingLoan = false
let loanAmount = 0
let currentComputer = {}

salaryElement.innerText = formatNumberToNOK(salary)
bankBalanceElement.innerText = formatNumberToNOK(bankBalance)

async function fetchComputers() {
  try {
    //Fetching computers from API
    fetch(baseURL + "computers")
      .then((response) => response.json())
      .then((data) => (computers = data))
      .then((computers) => addComputersToSelection(computers))
  } catch (error) {
    console.error("Error fetching computers")
  }
}

const addComputersToSelection = (computers) => {
  computers.forEach(
    (computer) => (computerSelect.innerHTML += computerOptionTemplate(computer))
  )

  // Updates UI with the first computer
  currentComputer = computers[0]
  infoContainer.innerHTML = computerInfoTemplate(computers[0], baseURL)
  specsList.innerHTML = computerSpecsTemplate(computers[0])
}

const handleSelectionChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex]
  infoContainer.innerHTML = computerInfoTemplate(selectedComputer, baseURL)
  specsList.innerHTML = computerSpecsTemplate(selectedComputer)
  currentComputer = selectedComputer
}

const loan = () => {
  let maxLoan = bankBalance * 2
  let userInput = Number(prompt("Enter amount to loan"))

  // Checks if requested loan can be
  const isLoanApproved = () => {
    if (userInput <= 0) {
      alert("Please enter a higher amount")
      return false
    } else if (isNaN(userInput)) {
      alert("Please enter amount as a number")
      return false
    } else if (userInput > maxLoan) {
      alert("It is not possible to loan more than double of your bankbalance")
      return false
    } else if (hasOutstandingLoan) {
      alert("Please pay the last loan")
      return false
    } else {
      return true
    }
  }

  // Gives loan
  if (isLoanApproved()) {
    bankBalance += userInput
    loanAmount += userInput
    hasOutstandingLoan = true
    alert(
      "Loan of " +
        formatNumberToNOK(loanAmount) +
        " kr is approved. Your balance is now: " +
        formatNumberToNOK(bankBalance) +
        " kr"
    )

    // Update UI
    repayBtn.hidden = false
    bankBalanceElement.innerText = formatNumberToNOK(bankBalance)
    outstandingLoanElement.innerText = formatNumberToNOK(loanAmount)
  }
}

const work = () => {
  salary += 100
  salaryElement.innerText = formatNumberToNOK(salary)
}

const transferToBank = () => {
  let deduction = salary * 0.1
  let restBalance = salary - deduction

  // If user has an outstanding loan, 10% of salary goes to loan
  if (hasOutstandingLoan) {
    loanAmount -= deduction
    bankBalance += restBalance
    outstandingLoanElement.innerText = formatNumberToNOK(loanAmount)
  } else {
    bankBalance += salary
  }

  salary = 0

  // Updates UI
  bankBalanceElement.innerText = formatNumberToNOK(bankBalance)
  salaryElement.innerText = formatNumberToNOK(salary)
}

const repay = () => {
  if (salary <= 0) {
    alert("You need to work more hours.. :/")
  } else {
    loanAmount -= salary

    // Adds the rest to the bank when salary is bigger than loan
    if (loanAmount <= salary) {
      bankBalance += Math.abs(loanAmount)
    }
    salary = 0

    // Updates UI
    bankBalanceElement.innerText = formatNumberToNOK(bankBalance)
    outstandingLoanElement.innerText = formatNumberToNOK(loanAmount)
    salaryElement.innerText = formatNumberToNOK(salary)

    // Resets variables and hides button when loan is payed in full
    if (loanAmount <= 0) {
      outstandingLoanElement.innerHTML = ""
      repayBtn.hidden = true
      hasOutstandingLoan = false
      loanAmount = 0
    }
  }
}

const buy = () => {
  if (bankBalance >= currentComputer.price) {
    alert("GZ with your new laptop " + currentComputer.title)
    bankBalance -= currentComputer.price
    bankBalanceElement.innerText = formatNumberToNOK(bankBalance)
  } else {
    alert("You do not have the sufficent funds to purchase this laptop")
  }
}

fetchComputers()

// Events
computerSelect.addEventListener("change", handleSelectionChange)
loanBtn.addEventListener("click", loan)
transferBtn.addEventListener("click", transferToBank)
repayBtn.addEventListener("click", repay)
workBtn.addEventListener("click", work)
buyBtn.addEventListener("click", buy)
