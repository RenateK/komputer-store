import { formatNumberToNOK } from "../utils.js"

export const computerInfoTemplate = (computer, baseURL) => {
  let imageUrl = baseURL + computer.image
  if (computer.id == 5) {
    // Corrects the wrong filetype in url from api. Image is png, not jpg.
    imageUrl = baseURL + "assets/images/5.png"
  }

  let template = `
  <img src=${imageUrl} alt=${computer.title} class="info-img">
  <div>
  <h2>${computer.title}</h2>
  <p>${computer.description}</p>
  <h3>${formatNumberToNOK(computer.price)}</h3>
  </div>
  `
  return template
}

export const computerOptionTemplate = (computer) => {
  let template = `
  <option value=${computer.id}>${computer.title}</option>
  `
  return template
}

export const computerSpecsTemplate = (computer) => {
  let template = ``
  let specs = computer.specs
  specs.forEach((spec) => {
    template += `<li>${spec}</li>`
  })
  return template
}
