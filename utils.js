// UTILS
export const formatNumberToNOK = (number) => {
  let NOK = Intl.NumberFormat("no-NO", {
    style: "currency",
    currency: "NOK",
  }).format(number)

  return NOK
}
